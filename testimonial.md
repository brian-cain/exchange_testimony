
# Exchange Testimony

We, the undersigned, stipulate that we are responsible for the operation of this cryptocurrency exchange and that our assessment of the qualities below are accurate to the best of our knowledge.

# Maturity Level 0

The items for level zero should be considered absolute bare minimum requirements for a cryptocoin exchange to operate with some degree of safety.

## Web Security Basics

 - [ ] User input is untrusted on the backend

 - [ ] User input is appropriately escaped

 - [ ] Input escaping: SQL injection tests verify that

 - [ ] Authentication: no cleartext passwords are ever stored

 - [ ] Authentication: password hashes include a salt

 - [ ] `X-Frame-Options` header used to prevent clickjacking

 - [ ] Insecure direct object references: TBD

 - [ ] Web pages are served exclusively with TLS (https)

 - [ ] Two-factor auth support is available, can be configured to gate logins, withdrawals

### Cross-Site Request Forgery

 - [ ] CSRF tokens are generated and checked for all user input


## Exchange Business

 - [ ] The exchange is registered as an entity with the relevant jurisdiction(s)

## Wallet security

 - [ ] Cold wallets exist for each currency

 - [ ] Transactions above a "reasonable" amount are required to be executed by interactive/human support team.

 - [ ] Wallet/node software is isolated from other wallets.


# Maturity Level 1

 - [ ] A bug bounty exists for the exchange operations

 - [ ] Remote access to hosted nodes is limited to only required exchange personnel and is audited.

## Settling trades

 - [ ] A test suite exists for corner cases of market/limit order places and cancellation.

## Wallet security

 - [ ] Cold wallets are stored on an airgapped computer or hardware wallet.  Transactions are created/signed there and moved to a hot wallet via sneakernet.

 - [ ] Multi-sig is used where available to protect cold wallets.


# Maturity Level 2

 - [ ] Account deposits held are insured against theft/loss

 - [ ] An incident response team exists for handling emergencies

 - [ ] Professional pentest audits are performed against the exchange at least annually

 - [ ] A process exists to periodically review third party software used by the exchange for vulnerabilities and security patches.  This includes operating systems, system libraries, web frameworks, cryptocoin wallets, and more.
