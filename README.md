
# Cryptocoin Exchange Testimonial

## What is this for?

This is designed to be a checklist for exchanges to publicly testify that they conform to some reasonable security standard processes.  The goal is to create a simple disclosure that will help everyone understand how seriously each exchange takes security.

## What's appropriate content?  How should this be used?

This should be a high level enumeration of clear, simple and explicit items that exchanges can use to rate themselves.  Unfortunately we cannot exhaustively list all potential risks.  We must focus on the major risks and confine the testimony to one or two pages worth of items.

Users or would-be users of an exchange should demand that exchange operators publish a completed copy of this testimonial for their exchange.  Exchange operators should take their testimony seriously under risk of fraud.  It would be reasonable for operators to qualify their testimony with a rationale regarding how they think they satisfy each item, or how it might not be applicable.  It would be ideal, they describe a way that the item can be independently confirmed where possible.

## Project Status

This testimonial is still under development.  Once we've released it, you should encourage exchanges that you use or plan to use to testify which Maturity Level they conform to.
